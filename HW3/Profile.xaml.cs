﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HW3
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Profile : ContentPage
    {
        public Profile()
        {
            InitializeComponent();
        }

        // Back Button pressed
        private void Button_Clicked(object sender, EventArgs e)
        {
            Navigation.PopAsync();
        }

        // Home Button pressed
        private void Button_Clicked_1(object sender, EventArgs e)
        {
            // Return to MainPage
            Navigation.PopToRootAsync();
        }

        // Student ID Card Button pressed
        private void Button_Clicked_2(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Card());
        }
    }
}