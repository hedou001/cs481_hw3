﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace HW3
{
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        // Creating a unique Clock page (so it is not destroyed when popped from the navigation)
        // Another solution would be to pass to the constructor a value but it is expensive to create a page
        private Clock clock = new Clock();
        public MainPage()
        {
            InitializeComponent();
        }

        // My Profile Button
        private async void Button_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Profile());
        }

        // Clock Button
        private async void Button_Clicked_1(object sender, EventArgs e)
        {
            await Navigation.PushAsync(clock);
        }
    }
}
